# DaBL

DESIGN AND BUILD LAB OPEN REPO
---------------------------------------------------------------------
In order to use this repo please conform to the following rules: 

1. Name the added file as lastname_item.fileextension
---------------------------------------------------------------------
Although we are committed to the philosophy of open source materials 
and sharing knowledge, crediting someone with their work is just as 
important.  This is for a couple of reasons.  First, the person who 
did all the effort for the creation (whether it's you or not) should 
be given credit where credit is due.  Second, violations of the rules
or further questions on the creations can be addressed by the CaTS of 
the DaBL or the manager this way without providing enough information 
to compromise the submitter should he/she/they wish to remain anonymous. 

2. This repo is for the unique creations of DaBL members
----------------------------------------------------------------------
This repo is not for those wishing to share potential project ideas 
(unless accompanied by one's own sketches/designs), creations of friends
that cannot be members, or general files.  This repo is to serve as a 
collection of finished projects* for which other DaBL members might 
learn from, attempt to recreate, and ask questions about.  This is due 
to the fact that all projects and files posted in the repo must be able to
be recreated within the lab in order to promote the educational aspect of 
the projects.  If you would like to share potential project ideas or 
something otherwise relevant to the DaBL, please send us an email at 
dabl@american.edu or post it on a third party website.  

3.  This repo is mainly for finished projects
----------------------------------------------------------------------
The repo is not to be a storage space for temporary project files or 
in-progress work.  Everything in the repo must hold some value for others,
therefore all finished projects are allowed.  However, if a project holds 
some partial importance (for example, a project with a self-driving RC car 
may hold a useful file for working a motor).  It is up to the submitter to 
determine if the content could be important or useful to others.  Any files 
violating this rule will be removed.  

4. Do not use the repo as a chance to critique or comment on other projects
------------------------------------------------------------------------



This repository is for sharing things with DaBL users and enthusiasts of topics covered in the Design and Build Lab.