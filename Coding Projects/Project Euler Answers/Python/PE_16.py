
def power_digit_sum():
    tot_sum = 0
    big_ass_num = 2 ** 1000
    for x in str(big_ass_num):
        tot_sum += int(x)
    return tot_sum

print(power_digit_sum())
