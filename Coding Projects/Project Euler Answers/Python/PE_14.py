

def collatz_chain():
    max_chain = []
    
    for x in range(1, 1000000):
        chain = [x]
        while x != 1:
            if (x%2 == 1):
                x = (3*x)+1
            else:
                x = x/2
            chain.append(x)
        if len(chain) > len(max_chain):
            max_chain = chain

    return max_chain


print(collatz_chain())
                    
