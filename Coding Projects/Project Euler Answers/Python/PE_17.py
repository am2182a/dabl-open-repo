

less_twenty_dic = {
        (0,):0,
        (1,2,6,10):3,
        (4,5,9):4,
        (3,7,8):5,
        (11,12):6,
        (15,16):7,
        (13,14,18,19):8,
        (17,):9
        }

tens_dic = {
    (0,):0,
    (4,5,6):5,
    (2,3,8,9):6,
    (7,):7
}

def ones_read(x):
    for k,v in less_twenty_dic.items():
        if (x in k):
            return v
        
def tens_read(x):
    for k,v in tens_dic.items():
        if ((x//10) in k):
            return v

def num_to_letter_size(size):
    tlc = 0
    
    for x in range(1, (size+1)):
        y=x
        if (x>99):
            tlc += ones_read(x//100) + 7
            if (x%100 != 0):
                tlc += 3
            x = x%100
        if (x>19):
            tlc+= tens_read(x)
            x = x%10
        if (x>=0):
            tlc += ones_read(x) 
        
    return (tlc + 11)

print(num_to_letter_size(999))

        
