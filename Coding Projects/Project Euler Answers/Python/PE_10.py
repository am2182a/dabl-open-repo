import math
import time
def primeSieve(sieveSize):

    sieve = [True] * sieveSize
    sieve[0] = False  
    sieve[1] = False

    for i in range(2, int(math.sqrt(sieveSize)) + 1):
        pointer = i * 2
        while pointer < sieveSize:
            sieve[pointer] = False
            pointer += i

    primes = 0
    for i in range(sieveSize):
        if sieve[i] == True:
            primes += i
    return primes


t0 = time.time()
print(primeSieve(2000000))
t1 = time.time()

print (t1 - t0)
