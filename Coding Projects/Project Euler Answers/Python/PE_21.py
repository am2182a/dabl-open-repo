

def divisor_sum():
    limit = 10000
    divisor_sum_list = {}
    for x in range(1, 10000):
        divisor_sum_list[x] = sum_divisors(x)
    return divisor_sum_list 

def amicable_numbers():
    total_sum = [];
    divisor_dic = divisor_sum()
    for k,v in divisor_dic.items():
        for l,u in divisor_dic.items():
            if ((k == u) and (v == l) and (k not in total_sum) and (k != v)):
                print(str(k) + "   " + str(sum_divisors(k)))
                print(str(l) + "   " + str(sum_divisors(l)))
                total_sum.append(k)
                total_sum.append(v)          
    return sum(total_sum)

def sum_divisors(x):
    sum_div = 0
    for y in range(1,x):
        if (x%y == 0):
            sum_div += y
    return sum_div



print(amicable_numbers())



