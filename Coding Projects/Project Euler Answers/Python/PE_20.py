def factorial(n):
    result = 1
    for x in range(1, n+1):
        result = result * x
    return result

def add_digits(n):
    tot_sum = 0
    for x in str(n):
        tot_sum += int(x)
    return tot_sum

print(add_digits(factorial(100)))
